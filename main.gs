
const contoller = new SpreadSheetAPIController();

function doGet(e) {
  try{
    let result = contoller.handleGet(e);
    return Response(result);
  } catch(error) {
    return Abort(error);
  }
}

function doPost(e) {
  contoller.handlePost(e);
}
