/**
 * スプレッドシート管理
 * @param targetSheetName:string
 */
function SpreadSheetDatabase(targetSheetName) {
  const _this = this;

  // targetSheetNameでシートを初期化
  const book = SpreadsheetApp.getActive();
  const sheet = book.getSheets().find(sheet => sheet.getName() === targetSheetName);
  if(!sheet){
    throw new Error('シートが存在しません');
  }

  // 1行目はカラムキー行
  _this.columnKeys = function () {
    return allRows().splice(0, 1)[0];
  };

  // 2行目以降はレコード行
  _this.allRecords = function () {
    const records = allRows();
    return records.slice(1, records.length);
  };

  // 全行取得
  function allRows() {
    return sheet.getDataRange().getValues();
  };

  // 行の挿入
  _this.insertRow = function(row) {
    if(!Array.isArray(row)){
      return;
    }
    return sheet.appendRow(row);
  };
}


