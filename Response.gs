
/**
 * @param data:object
 * @return output:TextOutput
 */
function Response(data) {
  return ContentService.createTextOutput(JSON.stringify(data, null, 2))
          .setMimeType(ContentService.MimeType.JSON);
}

/**
 * @param error:Error
 * @param etc:any
 * @return output:TextOutput
 */
function Abort(error, etc) {
  const errorResponse = {
    error: {
      message:error.message,
    }
  }
  errorResponse.etc = etc;
  return Response(errorResponse);
}