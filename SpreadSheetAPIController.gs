

/**
 * APIコントローラー
 */
function SpreadSheetAPIController() {
  const _this = this;
  let db;

  /**
   * 初期化
   * @param e:request変数
   * @param method:string
   */
  function initDB(e, method='GET') {
    let sheet = getTargetSheetNameFromData(
      parseDataWithMethodName(e, method)
    );
    // 指定スプレッドシートの管理インスタンスを生成
    db = makeTargetSpreadSheetDatabase(
      sheet
    );
  }

  /**
   * @method GET
   */
  _this.handleGet = function (e) {
    initDB(e, 'get');
    return AppUtils.convertRowsToAPIResult(
      db.allRecords(),
      db.columnKeys()
    );
  }
  /**
   * @method POST
   */
  _this.handlePost = function(e) {
    initDB(e, 'post');
    db.insertRow(
      AppUtils.convertPostDataToRow(
        AppUtils.parsePostData(e),
        db.columnKeys()
      )
    );
  }

  /** シート名が見つからないかった際の使用シート名 */
  const DEFAULT_SHEET_NAME = 'シート1';

  /**
   * 指定されたシート名を取得
   * @param data:object
   * @return sheet:string
   */
  function getTargetSheetNameFromData(data) {
    return (data && data.sheet) ? data.sheet : DEFAULT_SHEET_NAME;
  }
  /**
   * 指定されたmethod名に対応してdataを取得
   * @param e:request変数
   * @param method:string
   * @return data:object
   */
  function parseDataWithMethodName(e, method = 'GET') {
    if(method.toLowerCase() == 'get') {
      return AppUtils.parseGetParams(e);
    }
    if(method.toLowerCase() == 'post') {
      return AppUtils.parsePostData(e);
    }
  }

  /**
   * 指定したスプレッドシートの管理インスタンスを作成
   * @return db:SpreadSheetDatabase
   */
  function makeTargetSpreadSheetDatabase(sheet) {
    return new SpreadSheetDatabase(sheet);
  }
}


/**
 * utilities
 */
const AppUtils = {

  parseGetParams(getE) {
    return getE.parameter;
  },
  parsePostData(postE) {
    return JSON.parse(postE.postData.getDataAsString());
  },
  convertPostDataToRow(postData, keys) {
    return keys.map(key=>{
      const value = postData[key];
      if (value instanceof Date) {
        return Utilities.formatDate(value, 'Asia/Tokyo', 'yyyy/MM/dd HH:mm:ss');
      }
      if (typeof value === 'object') {
        return JSON.stringify(value);
      }
      return value;
    });
  },
  convertRowsToAPIResult(rows, keys) {
    const result = rows.map(row => {
      const obj = {};
      row.map((item, index) => {
        obj[String(keys[index])] = String(item);
      });
      return obj;
    });
    return result;
  },
}
